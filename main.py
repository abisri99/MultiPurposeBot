# import imp
import logging,os
from unittest import result

import telegram
import plugins.morseCode as morseCode
import plugins.dictionary as dictionary
import plugins.movies as movies
import plugins.untrack as untrack
import plugins.speed_test as speed
import plugins.url_shorten as shorten
import plugins.wallpapers as walls
from telegram import Update, ForceReply, InlineQueryResultArticle, ParseMode, InputTextMessageContent
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext, InlineQueryHandler
from telegram.utils.helpers import escape_markdown
from uuid import uuid4
import validators
# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context.
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    update.message.reply_markdown_v2(
        "Hi " + user.mention_markdown_v2()+",\nUse /help to know more about this bot \.")


def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text("""This is a bot developed by @abisri99.

                                Please expect a lot of bugs, consider this a beta version.
                                Try these commands:
                                /morse
                                /unmorse
                                /meaning
                                /imdb
                                /untrack

                                You can also use the untrack via inline mode. """)

def getMorse(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    # print(update.message.reply_to_message.text)
    if (update.message.reply_to_message == None):
        update.message.reply_text(
            "Reply this command to a message, to convert into morse code.")
    else:
        res = morseCode.morsify(update.message.reply_to_message.text)
        update.message.reply_text(res)


def getUnMorse(update: Update, context: CallbackContext) -> None:
    if (update.message.reply_to_message == None):
        update.message.reply_text(
            "Reply this command to a message with morse code, to convert into english message.")
    else:
        res = morseCode.deMorsify(update.message.reply_to_message.text)
        # print(res)
        update.message.reply_text(res)


def meaning(update: Update, context: CallbackContext) -> None:
    if (update.message.reply_to_message == None):
        update.message.reply_text(
            "Reply this command to a message with english word, to get the meaning.")
    else:
        res = dictionary.dictionary(update.message.reply_to_message.text)
        update.message.reply_text(res)


def imdb(update: Update, context: CallbackContext) -> None:
    if (update.message.reply_to_message == None):
        update.message.reply_text(
            """Reply this command to a message with name of movie/series, to get the ratings.
            Make sure spelling is correct.""")
    else:
        res = movies.getRatings(update.message.reply_to_message.text)
        update.message.reply_photo(
            res[0], caption=res[1], parse_mode='MarkdownV2')


def manual_untrack(update: Update, context: CallbackContext) -> None:
    if (update.message.reply_to_message == None):
        update.message.reply_text("""Reply this command to a message with a URL, and get their Untracked version.
                                    Use /help to check supported URLs. """)
    else:
        res = untrack.unTrack(update.message.reply_to_message.text)
        if res[0] != 404:
            update.message.reply_text("Untracked URL: "+res[0])
        else:
            update.message.reply_text(res[1])

def shortUrl(update: Update, context: CallbackContext) -> None:
    if (update.message.reply_to_message == None):
        update.message.reply_text("""Reply this command to a message with a URL, and get a shortened URL. """)
    else:
        res = shorten.shorten(update.message.reply_to_message.text)
        update.message.reply_text(res)

def wallpapers(update: Update, context: CallbackContext):
    if (update.message.reply_to_message == None):
        update.message.reply_text("""Reply this command to a query, and get a related wallpapers. """)
    else:
        images = []
        res = walls.wallpapers(update.message.reply_to_message.text)
        for url in res:
            images.append(telegram.InputMediaDocument(url))
        update.message.reply_media_group(media=images)


# def speedtest(update: Update, context: CallbackContext) -> None:
#     temp_msg = update.message.reply_text("Checking speed.....")
#     update.message.reply_photo(speed.speedtest(),"Speedtest Results")
#     temp_msg.delete()        


def inlinequery(update: Update, context: CallbackContext) -> None:
    """Handle the inline query."""
    query = update.inline_query.query

    if query == "" or validators.url(query) == False:
        return
    res = untrack.unTrack(query)
    if res[0] == 404:
        return
    results = [
        InlineQueryResultArticle(
            id=str(uuid4()),
            title=res[1],
            thumb_url=res[2],
            description=res[3],
            url=res[0],
            input_message_content=InputTextMessageContent(res[0]),
        ),
    ]

    update.inline_query.answer(results)


def main() -> None:
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(os.environ.get('bot_token'))

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("morse", getMorse))
    dispatcher.add_handler(CommandHandler("unmorse", getUnMorse))
    dispatcher.add_handler(CommandHandler("meaning", meaning))
    dispatcher.add_handler(CommandHandler("imdb", imdb))
    dispatcher.add_handler(CommandHandler("untrack", manual_untrack))
    dispatcher.add_handler(CommandHandler("shortUrl", shortUrl))
    dispatcher.add_handler(CommandHandler("walls", wallpapers))
    dispatcher.add_handler(InlineQueryHandler(inlinequery))
    # on non command i.e message - echo the message on Telegram
    # dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
