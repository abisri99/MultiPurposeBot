from urllib.parse import urlparse
import os

def unTrack(link=""):
    if link == "":
        return "Please enter a URL"
    else:
        untracked_url = ""
        parsed_url = urlparse(link)
        if parsed_url.netloc == "twitter.com" or parsed_url.netloc == "www.twitter.com":
            untracked_url =parsed_url._replace(netloc=os.environ.get("twitter_alt")).geturl()
            return untracked_url,"Nitter",os.environ.get("twitter_logo"), "Alternative Twitter front-end"
        elif parsed_url.netloc == "youtube.com" or parsed_url.netloc == "www.youtube.com" or parsed_url.netloc == "youtu.be":
            untracked_url =parsed_url._replace(netloc=os.environ.get("yt_alt")).geturl()
            return untracked_url, "Piped", os.environ.get("yt_logo"),"Alternative YouTube front-end"
        elif parsed_url.netloc == "reddit.com" or parsed_url.netloc == "www.reddit.com":
            untracked_url =parsed_url._replace(netloc=os.environ.get("reddit_alt")).geturl()
            return untracked_url,"Libreddit",os.environ.get("reddit_logo"),"Alternative Reddit front-end"
        elif parsed_url.netloc == "medium.com" or parsed_url.netloc == "www.medium.com":
            untracked_url =parsed_url._replace(netloc=os.environ.get("medm_alt")).geturl()
            return untracked_url,"medium",os.environ.get("medm_logo"),"Alternative Medium front-end"
        else:
            return 404,"Unsupported URL"
        
