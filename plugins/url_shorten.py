import requests
def shorten(url):
    import requests

    files = {
    'shorten': (None, url),
}

    response = requests.post('https://ttm.sh', files=files)
    if response.status_code == 200:
        return response.text
    else:
        return "Either URL given is invalid or Shorten API down."

# print(shorten("quit"))