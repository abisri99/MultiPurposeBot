import speedtest

def check_speed():
    speed = speedtest.Speedtest()
    speed.get_best_server()
    speed.download(threads=None)
    speed.upload(threads=None)
    speed.results.share()
    return speed.results.dict().get("share")

# print(check_speed())