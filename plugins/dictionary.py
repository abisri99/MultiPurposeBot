import requests
def dictionary(text=""):
    if text == "":
        return "Please use the command on a word."
    elif len(text.split(" ")) > 1:
        return "Please use words, not sentences."
    else:
        dic_url = "https://api.dictionaryapi.dev/api/v2/entries/en/"+text
        data = requests.get(dic_url).json()
        if isinstance(data, dict):
            return data["message"]
        else:
            # data = data[0]
            res = "Word: " + text + "\n\n" + "Meanings: \n\n" 
            count = 1  
            for obj in data: 
                if count == 4:
                    return res
                else:
                    res += str(count) + ") "+ obj["meanings"][0]["definitions"][0]["definition"]+"\n\n"
                count += 1 
            return res.strip()

# print(dictionary("Pet"))