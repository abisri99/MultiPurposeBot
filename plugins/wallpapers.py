import requests

def wallpapers(query):

    headers = {
        'accept': 'application/json',
    }

    params = {
        'query': query,
        'limit': '5',
    }

    response = requests.get('https://api.safone.tech/wall', params=params, headers=headers)
    if response.status_code == 200:
        images = []
        for data in response.json()['results']:
            images.append(data["imageUrl"])
    return images
# print(wallpapers("IRONMAN"))