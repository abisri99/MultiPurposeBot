import json

def morsify(text):
    try:
        morse_file = open('./static/morse-data.json','r')
        data = json.load(morse_file)
        text = text.lower()
        text = text.replace("\n"," ")
        res = ""
        for letter in text:
            res = res + " " +data[letter]
        # print("Morse Code: \n-> " + res.strip())
        return res.strip()
    except Exception as e:
        return "One or more unsupported charecters detected."

def deMorsify(text):
    try:
        unmorse_file = open('./static/unmorse-data.json','r')
        data = json.load(unmorse_file)
        arr = text.split(' ')
        res = ""
        for letter in arr:
            res = res+data[letter]
        # print("Morse Code: \n-> " + res.strip())
        return res.strip()
    except Exception as E:
        return "Please check the morse code. If You think it is correct, inform @abisri99 about this."
# deMorsify(".... .. / .- -... .. .-.. .- ... ....")
# print(morsify("Jai#Hind"))